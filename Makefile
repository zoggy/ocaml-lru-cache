#################################################################################
#                OCaml-Lru-cache                                                #
#                                                                               #
#    Copyright (C) 2016-2024 INRIA All rights reserved.                         #
#    Author: Maxence Guesdon, INRIA Saclay                                      #
#                                                                               #
#    This program is free software; you can redistribute it and/or modify       #
#    it under the terms of the GNU General Public License as                    #
#    published by the Free Software Foundation, version 3 of the License.       #
#                                                                               #
#    This program is distributed in the hope that it will be useful,            #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               #
#    GNU General Public License for more details.                               #
#                                                                               #
#    You should have received a copy of the GNU General Public                  #
#    License along with this program; if not, write to the Free Software        #
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   #
#    02111-1307  USA                                                            #
#                                                                               #
#    As a special exception, you have permission to link this program           #
#    with the OCaml compiler and distribute executables, as long as you         #
#    follow the requirements of the GNU GPL in regard to all of the             #
#    software in the executable aside from the OCaml compiler.                  #
#                                                                               #
#    Contact: Maxence.Guesdon@inria.fr                                          #
#                                                                               #
#################################################################################

SHELL := /bin/bash
#
all:
	dune build

install:
	dune build @install
	dune install

doc: all
	dune build @doc

runtest:
	dune runtest

webdoc: doc
	rm -fr public/refdoc
	cp -r _build/default/_doc/_html public/refdoc
	tail -n +2 README.md | pandoc -f markdown -t html -s --metadata title="OCaml-Lru-cache" > public/index.html

# archive :
###########
archive:
	$(eval VERSION=`git describe | cut -d'-' -f 1`)
	git archive --worktree-attributes --prefix=ocaml-lru-cache-$(VERSION)/ $(VERSION) | gzip > public/releases/ocaml-lru-cache-$(VERSION).tar.gz

# Cleaning :
############
clean:
	dune clean

# headers :
###########
HEADFILES:=$(shell ls Makefile lib/*.ml{,i} test/*.ml)
.PHONY: headers noheaders
headers:
	echo $(HEADFILES)
	headache -h .header -c .headache_config $(HEADFILES)

noheaders:
	headache -r -c .headache_config $(HEADFILES)

