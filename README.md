# ocaml-lru-cache

ocaml-lru-cache is a simple OCaml implementation of a cache using
the [Least Recently Used (LRU)](https://en.wikipedia.org/wiki/Cache_algorithms)
strategy.

## Development

Development is [hosted on Framagit](https://framagit.org/zoggy/ocaml-lru-cache).

## Installation

Install the library via [OPAM][opam]:

[opam]: http://opam.ocaml.org/

```bash
opam install lru_cache
```

### Documentation

Reference documentation is [here](https://zoggy.frama.io/ocaml-lru-cache/refdoc/lru_cache/).

### Releases

- [0.4.0](https://zoggy.frama.io/ocaml-lru-cache/releases/ocaml-lru-cache-0.4.0.tar.bz2)
- [0.3.0](https://zoggy.frama.io/ocaml-lru-cache/releases/ocaml-lru-cache-0.3.0.tar.gz)
- [0.2.0](https://zoggy.frama.io/ocaml-lru-cache/releases/ocaml-lru-cache-0.2.0.tar.gz)
- [0.1.0](https://zoggy.frama.io/ocaml-lru-cache/releases/ocaml-lru-cache-0.1.0.tar.gz)

### Examples

See the `test/test_lwt.ml` for an example using Lwt to compute cached values.
To compile the example, [Lwt](http://ocsigen.org/lwt/) must be installed.

```bash
make runtest
```
## License

BSD3, see LICENSE file for its text.
