(*********************************************************************************)
(*                OCaml-Lru-cache                                                *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

module I = struct
    type t = int
    let compare x y = x - y
    let witness = 0
  end
module Cache = Lru_cache.Make(I)

let (cache : string Lwt.t Cache.t) = Cache.init 3

let print str = Lwt_io.write_line Lwt_io.stdout str

let f n =
  let%lwt () = print (Printf.sprintf "computing f %d" n) in
  let%lwt () = Lwt_unix.sleep 1. in
  Lwt.return (string_of_int n)

let run () =
  let go n =
    let%lwt () = Lwt_unix.sleep 0.1 in
    let%lwt () =
      if Cache.in_cache cache n then
        print (Printf.sprintf "f %d is in cache" n)
      else
        print (Printf.sprintf "f %d is in not in cache" n)
    in
    match%lwt Cache.get cache n f with
    | "3" ->
        let removed = Cache.remove cache 3 in
        let msg = if removed then "was removed" else "was not removed (it's ok)" in
        Lwt.return (n, msg)
    | res -> Lwt.return (n, res)
  in
  Lwt_list.map_p go
    [ 1 ; 2 ; 3 ; 3 ; 4 ; 1 ; 1 ; 1 ; 2 ; 5 ; 5 ; 5 ; 2 ; 3 ]

let l = Lwt_main.run (run ())
let () = List.iter
  (fun (n, str) -> print_endline (Printf.sprintf "%d -> %s" n str))
  l




    