(*********************************************************************************)
(*                OCaml-Lru-cache                                                *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

module type Key =
  sig
    type t
    val compare : t -> t -> int
    val witness : t
  end

module type S =
  sig
    type key
    type 'a monad
    type 'a t

    val init : ?validate:('a monad -> bool monad) -> int -> 'a t
    val in_cache : 'a t -> key -> bool
    val get : 'a t ->
      ?validate:('a monad -> bool monad) -> key -> (key -> 'a monad) -> 'a monad
    val remove : 'a t -> key -> bool
  end

module type Monad =
  sig
    type 'a t
    val bind: 'a t -> ('a -> 'b t) -> 'b t
    val return : 'a -> 'a t
  end

module Make_with_monad (Mo:Monad) (K:Key) =
  struct
    type key = K.t

    module M = Map.Make(K)
    type 'a v = { v: 'a Mo.t; pos: int ref }
    type key_node = { key : key; pos: int ref }

    type 'a t =
        { mutable map: 'a v M.t ;
          keys: key_node array ;
          validate : 'a Mo.t -> bool Mo.t;
        }
    type 'a monad = 'a Mo.t

    let init ?(validate=fun _ -> Mo.return true) size =
      { map = M.empty ;
        keys = Array.make size {key = K.witness ; pos = ref (-1)} ;
        validate ;
      }

    let remove_last t =
      let len = Array.length t.keys in
      let k = t.keys.(len - 1) in
      t.map <- M.remove k.key t.map

    let insert t key v =
      let size =
        let size = M.cardinal t.map in
        if size >= Array.length t.keys then
          ( remove_last t ; size - 1)
        else
          size
      in
      let pos = ref 0 in
      Array.blit t.keys 0 t.keys 1 size ;
      for i = 1 to size do
        t.keys.(i).pos := i ;
      done;
      t.keys.(0) <- { key ; pos } ;
      t.map <- M.add key { v ; pos } t.map

    let remove t key =
      match M.find_opt key t.map with
      | None -> false
      | Some { v ; pos } ->
          t.map <- M.remove key t.map;
          let size = M.cardinal t.map in
          for i = !pos + 1 to size do
            t.keys.(i).pos := i - 1
          done;
          Array.blit t.keys (!pos+1) t.keys !pos (size - !pos);
          t.keys.(size) <- {key = K.witness ; pos = ref (-1)} ;
          true

    let to_head t pos =
      match !pos with
        0 -> ()
      | p ->
          let v = t.keys.(p) in
          Array.blit t.keys 0 t.keys 1 p ;
          for i = 1 to p do
            t.keys.(i).pos := i ;
          done;
          v.pos := 0 ;
          t.keys.(0) <- v

    let in_cache t k =
      match M.find_opt k t.map with
      | None -> false
      | _ -> true

    let get t ?(validate=t.validate) k f =
      match M.find_opt k t.map with
      | None ->
          let v = f k in
          Mo.bind (validate v)
            (function
             | true -> insert t k v; v
             | false -> v)
      | Some { v ; pos } -> to_head t pos; v

  end

module No_monad =
  struct type 'a t = 'a let bind x f = f x let return x = x end

module Make (K:Key) = Make_with_monad(No_monad) (K)
